




<!doctype php>
<html lang="pt-br">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <title>BEM SEGUROS</title>
  <link rel = "icon" href =  
"./src/favicon.png" 
        type = "image/x-icon"> 


  <!-- Bootstrap -->
  <link rel="stylesheet" href="./css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="css/custom.css">

  
</head>

<body>



<?php

 if( isset( $_POST['submit'] ) ) { 
  $nome = $_POST['nome']; 
  $cpf = $_POST['cpf']; 
  $data = $_POST['data'];
  $email = $_POST['email'];
  $plano = $_POST['submit'];

}
$nome_array = explode(" ", $nome);

$fp = fopen('saida.csv','a');
fputcsv($fp,array($cpf,$nome,$data,$email,$plano))

//var_dump($nome_array);
?>


<div class="container col">
<!-- Header -->
  <nav class="navbar navbar-expand-md navbar-light bg-white"> <!--Expands when screen reach md size-->
    <div class="container">
      <div class="col-3">
      <img src="./src/LogoBem.png" class="img-fluid" alt="Responsive image" style="width:100%;min-width:10em; min-height: 3em">
      </div>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar2" aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon my-toggler"></span>
      </button>

      <div class="collapse navbar-collapse" id="collapsingNavbar2">

        <ul class="navbar-nav ml-auto  justify-content-end ">

          <li class="nav-item">
            <a class="nav-link" href="https://www.bempromotora.com.br/sobre-a-bem">SOBRE A BEM </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://www.bempromotora.com.br/encontre-uma-loja-bem">ENCONTRE UMA LOJA</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="https://www.bempromotora.com.br/atendimento">ATENDIMENTO</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="https://www.bempromotora.com.br/blog-bem">BLOG</a>
          </li>
        </ul>

      </div>
    </div>
  </nav>
<!--/Header -->

<!--Text title -->
  <div class="row my-2 justify-content-center ">
    <?php echo "<h2 class=mx-4 text-responsive> Solicitação Recebida </h2>";?>
   

  </div>
<!--/Text title -->

<!--Presentation Text -->
  <div class="row ml-2 my-2 justify-content-center text-center">
  <?php  echo "<p class=text-responsive2 >" . $nome_array[0] . " entraremos em contato no e-mail: ". $email . "</p>";?>  
 <!-- <p class=text-responsive2>Texto de apresentação</p> --> 
 </div>
<!--/Presentation Text -->

</div>


<!-- Footer -->
  <footer class="page-footer fixed-bottom font-small blue">


    <!-- Copyright -->
    <div class="footer-copyright  text-center py-3">© 2019 Copyright:
      <a href="https://www.bempromotora.com.br">Bempromotora.com.br</a>
    </div>
    <!-- Copyright -->

  </footer>
<!-- /Footer -->


<!--scripts -->
<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
<script src="./js/bootstrap.min.js"></script>





</body>

</html>