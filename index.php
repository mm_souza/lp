<!doctype php>
<html lang="pt-br">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Seguros de Acidentes Pessoais Premiado</title>

  <link rel="icon" href="./src/favicon.png" type="image/x-icon">



  <!-- Bootstrap -->
  <link rel="stylesheet" href="./css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="css/custom.css">
  <script src='https://kit.fontawesome.com/a076d05399.js'></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>

<?php

$row = 1;
$data;
if (($handle = fopen("./users/usuario1.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        if($_GET['key'] == $data[0]){
            $nome=$data[1];
            $nascimento = $data[2];
            $cpf = $data[3];
            $email = $data[4];
      }
      
        
        
    }
    fclose($handle);
}

//var_dump($argv);
//var_dump($_GET['key']); 
    
    //echo "<p> $num campos na linha $row: <br /></p>\n";
    //$row++;
    //for ($c=0; $c < $num; $c++) {
    //   echo $data[$c] . "<br />\n";
    //}
   
    //var_dump($argv);
    //var_dump($_GET['key']);
  
  ?>




<!-- Header -->

<div class="container w-auto" >
  <nav class="navbar  navbar-expand-md navbar-light bg-white ">
    <!--Expands when screen reach md size-->
    <div class="container">
      <div class="col-1">
        <img src="./src/LogoBem.png" class="img-fluid" alt="Responsive image" style="width:100%;min-width:10em; min-height: 3em">
      </div>

      <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#collapsingNavbar2" aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon my-toggler"></span>
      </button>


      <div class="collapse navbar-collapse justify-content-end " id="collapsingNavbar2">
        
        <ul class="navbar-nav   float-right">
          <li class="nav-item  ">
            <a class="nav-link" href="https://www.bempromotora.com.br/sobre-a-bem">SOBRE A BEM </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="https://www.bempromotora.com.br/encontre-uma-loja-bem">ENCONTRE UMA LOJA</a>
          </li>
          <li class="nav-item ">
            <a class="nav-link " href="https://www.bempromotora.com.br/atendimento">ATENDIMENTO</a>
          </li>
          <li class="nav-item ">
            <a class="nav-link " href="https://www.bempromotora.com.br/blog-bem">BLOG</a>
          </li>
        </ul>
       
      </div>
    </div>
  </nav>
<!--/Header -->

<!--Text title -->
  <div class="row mt-4 my-2 justify-content-center text-center d-none d-md-block">
    <h2 class="mx-4 text-responsive"> Seguro Acidentes Pessoais Premiado </h2>
  </div>

  <div class="row mt-4 my-2 justify-content-center text-center  d-block d-md-none">
    <h2 class="mx-4 text-responsive"> Seguro Acidentes <br> Pessoais Premiado </h2>
  </div>


<!--/Text title -->

<!--Presentation Text -->
  <div class="row mx-4 my-2 text-justify justify-content-center">
    <p class=text-responsive2>O Seguro AP Premiado é um seguro de vida de Acidentes
      pessoais para pessoa física, com o objetivo de garantir proteção financeira ao segurado
      e/ou sua família, na ocorrência de eventos de Morte Acidental ou Invalidez Permante Total por Acidente.
    </p>
  </div>
<!--/Presentation Text -->

<!--Text title -->
<div class="row mt-4 my-2 justify-content-center text-center ">
    <h2 class="mx-4 text-responsive" style="font-size: calc(65% + 1vw + 1vh);font-weight: 800"> Formulário de Solicitação </h2>
    
  </div>
<!--/Text title -->


<!--Input Form Area -->
  <form class="form-horizontal" role="form" method="post" action="formprocess.php">
    <div class="row justify-content-center mt-4 mx-1">

      <div class="form-group col-md-5">
        <label for="NOME" class=labeltext>NOME</label>
        <input type="text" class="form-control" id="nome" name="nome" aria-describedby="NOME" placeholder="NOME" value="<?php echo $nome ?>" required>
      </div>

      <div class="form-group col-md-5">
        <label for="data" class=labeltext>DATA DE NASCIMENTO</label>
        <input type="date" class=" form-control " id="data" name="data" placeholder="Data de Nascimento" value="<?php echo $nascimento ?>" required>
      </div>

    </div>
    <div class="row justify-content-center mx-1">

      <div class="form-group col-md-5 ">
        <label for="CPF" class=labeltext>CPF</label>
        <input type="text" class="form-control" id="CPF" name="cpf" placeholder="CPF" value="<?php echo $cpf ?>" required>
      </div>

      <div class="form-group col-md-5 ">
        <label for="EMAIL" class=labeltext>EMAIL</label>
        <input type="email" class="form-control" id="EMAIL" name="email" placeholder="EMAIL" value="<?php echo $email ?>" required>
      </div>


      
    </div>
    <div class="row justify-content-center mt-4 mx-4 d-none">
    <button class="btn btn-primary enviar"  id="enviar" name="submit" type="submit" value="enviar">Enviar  </button>
    </div>


<!--Carousel Head Line -->
    <div class="row my-5  justify-content-center text-center ">

      <h2 class="mx-4 text-responsive3 " style="font-size: calc(65% + 1vw + 1vh);font-weight: 800"> Módulos de Contratação <br> Seguro AP Premiado</h2>

    </div>
<!--/Carousel Head Line -->


<!--Carousel for big screens -->
<!--Carousel prev button  -->
       <div class="row">
      <div class="col-1  offset-1 clearfix d-none d-md-block  ">
        <a class="carousel-control-prev" href="#multi-item-example" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
      </div>
<!--/Carousel prev button  -->

      <div class="col-8  text-center clearfix d-none d-md-block">
<!--visible just in big screens -->
        <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">
          <div class="carousel-inner" role="listbox">

      <!-- Card 1 -->
            <div class="carousel-item active">
              <div class="row">
                <div class="col-md-6">
                  <div class="card mb-auto">
                    <div class="card-body">
                      <h1 class="card-title" style="font-size:2vw;">R$ 12,90/mês </h1>
                      
                      <h4 class="mt-1 text-info" style="font-size:2.5vw;">Coberturas</h4>
                      
                      <table class="table table-borderless text-center text-align-middle ">
                        <thead>
                          <tr >
                            <th scope="col">Coberturas</th>
                            <th scope="col">Indenização</th>
                            </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td style="color:black">Morte Acidental</td>
                            <td style="color:black"> R$ 10.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Invalidez Permanente <br> Total por Acidente</td>
                            <td style="color:black">R$ 10.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Serviço Assistência <br> Funeral</td>
                            <td style="color:black">R$ 3.300 </td>
                          </tr>
                          </tbody>
                      </table>
                      <h4 class="mt-1 text-info" style="font-size:2.5vw;">Benefícios</h4>
                        
                      <table class="table table-borderless text-center ">
                        <tbody>
                          <tr>
                            <td style="color:black">Sorteio</td>
                            <td class="text-nowrap">1 xR$ 10.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Cesta Básica</td>
                            <td style="color:black"><i class="fa fa-close"></i></td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência desconto <br> farmácia </td>
                            <td style="color:black"><i class="fa fa-close"></i></td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência domiciliar <br> Funeral</td>
                            <td style="color:black"><i class="fa fa-close"></i></td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência Veículo <br>Passeio</td>
                            <td style="color:black"><i class="fa fa-close"></i></td>
                          </tr>
                        </tbody>
                      </table>

                      <button class="btn btn-primary" id="submit" name="submit" type="submit" value="Plano1">Tenho Interesse  </button>
                    </div>
                  </div>
                </div>

<!--Card comentado para guardar formato inicial-->
                <!-- <div class="col-md-4  ">
                  <div class="card mb-auto">
                    <div class="card-body">
                      <h1 class="card-title" style="font-size:3.5vw;">R$ 100.000 </h1>
                      <h6 class="card-subtitle mb-2"> Indenização</h6>


                      <h1 class="text-info" style="font-size:3.5vw;">R$ 145 </h1>
                      <div class="row justify-content-center">
                        <h7 class="card-subtitle mb-2 "> Valor Mensal (estimativa)</h7>
                      </div>
                      <img class="img my-4" src="./src/1-1-b.png" style=width:40% alt="Card image cap">

                      <p>Idade - 40 anos</p>
                      <p>Perfil - Dentista</p>
                      <h4 style="font-size:2.5vw;">Coberturas</h4>
                      <p>Doenças Graves</p>
                      <p>SAF individual</p>
                      <p>Morte Natural e por Acidente</p>

                      <button class="btn btn-primary" id="submit" name="submit" type="submit" value="Send">Tenho Interesse  </button>
                    </div>
                  </div>
                </div> -->


<!--Card 2 -->
                <div class="col-md-6 ">
                  <div class="card mb-auto">
                  <div class="card-body">
                      <h1 class="card-title" style="font-size:2vw;">R$ 18,90/mês </h1>
                      
                      <h4 class="mt-1 text-info" style="font-size:2.5vw;">Coberturas</h4>
                      
                      <table class="table table-borderless text-center ">
                        <thead>
                          <tr >
                            <th scope="col">Coberturas</th>
                            <th scope="col">Indenização</th>
                            </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td style="color:black">Morte Acidental</td>
                            <td style="color:black">R$ 30.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Invalidez Permanente <br> Total por Acidente</td>
                            <td style="color:black">R$ 30.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Serviço Assistência <br> Funeral</td>
                            <td style="color:black">R$ 3.300 </td>
                          </tr>
                          </tbody>
                      </table>
                      <h4 class="mt-1 text-info" style="font-size:2.5vw;">Benefícios</h4>
                        
                      <table class="table table-borderless text-center ">
                                         
                      <tbody>
                          <tr>
                            <td style="color:black">Sorteio</td>
                            <td class="text-nowrap">4 x R$ 15.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Cesta Básica</td>
                            <td style="color:black">6 x R$ 250 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência desconto <br> farmácia </td>
                            <td style="color:black"><i class='fa fa-check'></i></td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência domiciliar <br> Funeral</td>
                            <td style="color:black"><i class="fa fa-close"></i></td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência Veículo <br>Passeio</td>
                            <td style="color:black"><i class='fa fa-check'></i></td>
                          </tr>
                        </tbody>
                      </table>
                     
                      <button class="btn btn-primary" id="submit" name="submit" type="submit" value="Plano2">Tenho Interesse  </button>
                    </div>
                  </div>
                </div>



              </div>

            </div>


            <div class="carousel-item ">

              <div class="row">
<!--card 3 -->
                <div class="col-md-6  ">
                  <div class="card mb-auto">
                    <div class="card-body">
                    <h1 class="card-title" style="font-size:2vw;">R$ 29,90/mês </h1>
                      
                      <h4 class="mt-1 text-info" style="font-size:2.5vw;">Coberturas</h4>
                      
                      <table class="table table-borderless text-center ">
                        <thead>
                          <tr >
                            <th scope="col">Coberturas</th>
                            <th scope="col">Indenização</th>
                            </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td style="color:black">Morte Acidental</td>
                            <td style="color:black">R$ 50.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Invalidez Permanente <br> Total por Acidente</td>
                            <td style="color:black">R$ 50.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Serviço Assistência <br> Funeral</td>
                            <td style="color:black">R$ 5.500 </td>
                          </tr>
                          </tbody>
                      </table>
                      <h4 class="mt-1 text-info" style="font-size:2.5vw;">Benefícios</h4>
                        
                      <table class="table table-borderless text-center ">
                        <tbody>
                          <tr>
                            <td style="color:black">Sorteio</td>
                            <td class="text-nowrap"> 4 x R$ 25.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Cesta Básica</td>
                            <td style="color:black">R$ 6 x 250 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência desconto <br> farmácia </td>
                            <td style="color:black"><i class='fa fa-check'></i></td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência domiciliar <br> Funeral</td>
                            <td style="color:black"><i class='fa fa-check'></i></td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência Veículo <br>Passeio</td>
                            <td style="color:black"><i class='fa fa-check'></i></td>
                          </tr>
                        </tbody>
                      </table>

                      <button class="btn btn-primary " id="submit" name="submit" type="submit" value="Plano3">Tenho Interesse  </button>
                    </div>
                  </div>
                </div>
<!--card 4 -->
                <div class="col-md-6 ">
                  <div class="card mb-auto">
                    <div class="card-body">
                    <h1 class="card-title" style="font-size:2vw;">R$ 49,90/mês </h1>
                      
                      <h4 class="mt-1 text-info" style="font-size:2.5vw;">Coberturas</h4>
                      
                      <table class="table table-borderless text-center ">
                        <thead>
                          <tr >
                            <th scope="col">Coberturas</th>
                            <th scope="col">Indenização</th>
                            </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td style="color:black">Morte Acidental</td>
                            <td style="color:black">R$ 100.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Invalidez Permanente <br> Total por Acidente</td>
                            <td style="color:black">R$ 100.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Serviço Assistência <br> Funeral</td>
                            <td style="color:black">R$ 5.500 </td>
                          </tr>
                          </tbody>
                      </table>
                      <h4 class="mt-1 text-info" style="font-size:2.5vw;">Benefícios</h4>
                        
                      <table class="table table-borderless text-center ">
                        <tbody>
                          <tr>
                            <td style="color:black">Sorteio</td>
                            <td class="text-nowrap">4 x R$ 25.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Cesta Básica</td>
                            <td style="color:black">6 x R$ 250 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência desconto <br> farmácia </td>
                            <td style="color:black"><i class='fa fa-check'></i></td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência domiciliar <br> Funeral</td>
                            <td style="color:black"><i class='fa fa-check'></i></td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência Veículo <br>Passeio</td>
                            <td style="color:black"><i class='fa fa-check'></i></td>
                          </tr>
                        </tbody>
                      </table>

                      <button class="btn btn-primary" id="submit" name="submit" type="submit" value="Plano4">Tenho Interesse  </button>
                    </div>
                  </div>
                </div>

              
              </div>

            </div>

          </div>

        </div>

      </div>
<!--/Carousel next button  -->
      <div class="col-1 clearfix d-none d-md-block ">
        <a class="carousel-control-next" href="#multi-item-example" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>


    </div>


<!--Carousel for big screens -->




<!--Carousel for small screens-->
    <!-- <div class="container">
     <div class="row">

       <div class="col clearfix d-block d-md-none justify-center ">
        <a class="carousel-control-prev" href="#carrocel_telas_pequenas" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carrocel_telas_pequenas" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
       <span class="sr-only">Next</span> </a>
      </div> 
     </div>
    </div> -->




<!-- carrocel definitions -->   
    <div class="row mt-1 pb-1" >

      <div class="col text-center clearfix d-block d-md-none" >
      
    
      
        <div id="carrocel_telas_pequenas"  class="carousel slide carousel-multi-item w-100" data-ride="carousel">
        
        <ol class="carousel-indicators" style=" margin-bottom:-30px ;">
        <li data-target="#carrocel_telas_pequenas" data-slide-to="0" class="active"></li>
        <li data-target="#carrocel_telas_pequenas" data-slide-to="1"></li>
        <li data-target="#carrocel_telas_pequenas" data-slide-to="2"></li>
        <li data-target="#carrocel_telas_pequenas" data-slide-to="3"></li>
        </ol>
       


          <div class="carousel-inner" role="listbox">

<!-- Card 1 -->
            <div class="carousel-item active">
              
            <div class="col-md-4">
              <div class="card mb-auto">
                <div class="card-body  ">
                  <h1 class="card-title" style="font-size:8vw;">R$ 12,90/mês </h1>
                      
                      <h4 class="text-info mb-auto">Coberturas</h4>
                      
                      <table class="table table-borderless text-center text-align-middle ">
                        <thead>
                          <tr>
                            <th scope="col">Coberturas</th>
                            <th scope="col">Indenização</th>
                            </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td style="color:black">Morte Acidental</td>
                            <td style="color:black">R$ 10.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Invalidez Permanente  Total por Acidente</td>
                            <td style="color:black">R$ 10.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Serviço Assistência  Funeral</td>
                            <td style="color:black">R$ 3.300 </td>
                          </tr>
                          </tbody>
                      </table>

                      

                      <h4 class="my-auto text-info" style="font-size:6vw;">Benefícios</h4>
                        
                      <table class="table table-borderless text-center text-align-middle  ">
                        <tbody>
                          <tr>
                            <td style="color:black">Sorteio</td>
                            <td class="text-nowrap">1 x R$ 10.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Cesta Básica</td>
                            <td style="color:black"><i class="fa fa-close"></i></td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência desconto farmácia </td>
                            <td style="color:black"><i class="fa fa-close"></i></td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência domiciliar Funeral</td>
                            <td style="color:black"><i class="fa fa-close"></i></td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência Veículo Passeio</td>
                            <td style="color:black"><i class="fa fa-close"></i></td>
                          </tr>
                        </tbody>
                      </table>

                    <button class="btn btn-primary" id="submit" name="submit" type="submit" value="Plano1">Tenho Interesse  </button>
                  </div>
                </div>
              
              </div>
            </div>
            
<!-- Card 2 -->
            <div class="carousel-item">
              <div class="col-md-4  ">
                <div class="card mb-auto">
                  <div class="card-body">
                  <h1 class="card-title" style="font-size:8vw;"> R$ 18,90/mês </h1>
                      
                  <h4 class="my-auto text-info">Coberturas</h4>
                      
                      <table class="table table-borderless text-center text-align-middle ">
                        <thead>
                          <tr >
                            <th scope="col">Coberturas</th>
                            <th scope="col">Indenização</th>
                            </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td style="color:black">Morte Acidental</td>
                            <td style="color:black">R$ 30.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Invalidez Permanente  Total por Acidente</td>
                            <td style="color:black">R$ 30.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Serviço Assistência  Funeral</td>
                            <td style="color:black">R$ 3.300 </td>
                          </tr>
                          </tbody>
                      </table>
                      <h4 class="my-auto text-info" style="font-size:6vw;">Benefícios</h4>
                        
                      <table class="table table-borderless text-center ">
                        <tbody>
                          <tr>
                            <td style="color:black">Sorteio</td>
                            <td class="text-nowrap">4 x R$ 15.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Cesta Básica</td>
                            <td style="color:black">6 x R$ 250 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência desconto  farmácia </td>
                            <td style="color:black"><i class='fa fa-check'></i></td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência domiciliar  Funeral</td>
                            <td style="color:black"><i class="fa fa-close"></i></td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência Veículo Passeio</td>
                            <td style="color:black"><i class='fa fa-check'></i></td>
                          </tr>
                        </tbody>
                      </table>

                    <button class="btn btn-primary" id="submit" name="submit" type="submit" value="Plano2">Tenho Interesse  </button>
                  </div>
                </div>
              </div>
            </div>
<!-- Card 3 -->
            <div class="carousel-item">
              <div class="col-md-4 ">
                <div class="card mb-auto">
                    <div class="card-body">
                    <h1 class="card-title" style="font-size:8vw;">R$ 29,90/mês </h1>
                      
                      <h4 class="my-auto text-info">Coberturas</h4>
                      
                      <table class="table table-borderless text-center text-align-middle ">
                        <thead>
                          <tr >
                            <th scope="col">Coberturas</th>
                            <th scope="col">Indenização</th>
                            </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td style="color:black">Morte Acidental</td>
                            <td style="color:black">R$ 50.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Invalidez Permanente  Total por Acidente</td>
                            <td style="color:black">R$ 50.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Serviço Assistência  Funeral</td>
                            <td style="color:black">R$ 5.500 </td>
                          </tr>
                          </tbody>
                      </table>
                      <h4 class="my-auto text-info" style="font-size:6vw;">Benefícios</h4>
                        
                      <table class="table table-borderless text-center ">
                        <tbody>
                          <tr>
                            <td style="color:black">Sorteio</td>
                            <td class="text-nowrap" >4 x R$ 25.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Cesta Básica</td>
                            <td style="color:black">6 x R$ 250  </td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência desconto  farmácia </td>
                            <td style="color:black"><i class='fa fa-check'></i></td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência domiciliar  Funeral</td>
                            <td style="color:black"><i class='fa fa-check'></i></td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência Veículo Passeio</td>
                            <td style="color:black"><i class='fa fa-check'></i></td>
                          </tr>
                        </tbody>
                      </table>

                      <button class="btn btn-primary" id="submit" name="submit" type="submit" value="Plano3">Tenho Interesse  </button>
                  </div>
                </div>
              </div>
            </div>
<!-- Card 4 -->
            <div class="carousel-item">
              <div class="col-md-4 ">
                <div class="card mb-auto">
                    <div class="card-body">
                    <h1 class="card-title" style="font-size:8vw;">R$ 49,90/mês </h1>
                      
                      <h4 class="my-auto text-info">Coberturas</h4>
                      
                      <table class="table table-borderless text-center text-align-middle ">
                        <thead>
                          <tr >
                            <th scope="col">Coberturas</th>
                            <th scope="col">Indenização</th>
                            </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td style="color:black">Morte Acidental</td>
                            <td style="color:black">R$ 100.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black;" >Invalidez Permanente  Total por Acidente</td>
                            <td style="color:black">R$ 100.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Serviço Assistência  Funeral</td>
                            <td style="color:black">R$ 5.500 </td>
                          </tr>
                          </tbody>
                      </table>
                      <h4 class="my-auto text-info" style="font-size:6vw;">Benefícios</h4>
                        
                      <table class="table table-borderless text-center ">
                        <tbody>
                          <tr>
                            <td style="color:black">Sorteio</td>
                            <td class="text-nowrap" >4 x R$ 25.000 </td>
                          </tr>
                          <tr>
                            <td style="color:black">Cesta Básica</td>
                            <td style="color:black">6 x R$ 250  </td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência desconto farmácia </td>
                            <td style="color:black"><i class='fa fa-check'></i></td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência domiciliar Funeral</td>
                            <td style="color:black"><i class='fa fa-check'></i></td>
                          </tr>
                          <tr>
                            <td style="color:black">Assistência Veículo Passeio</td>
                            <td style="color:black"><i class='fa fa-check'></i></td>
                          </tr>
                        </tbody>
                      </table>
                    <button class="btn btn-primary" id="submit" name="submit" type="submit" value="Plano4">Tenho Interesse  </button>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </div>
  </form>
  </div>
  </div>
<!--/Carousel for small screens-->

<!-- Footer -->
  <footer class="page-footer font-small blue">

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2019 Copyright:
      <a href="https://www.bempromotora.com.br">Bempromotora.com.br</a>
    </div>
    <!-- Copyright -->

  </footer>
<!-- /Footer -->


</div>

  <!--scripts -->
  <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
  <script src="./js/bootstrap.min.js"></script>





</body>

</html>